import express from 'express';
import routesLoader from "./routesLoader";
import DatabaseConnection from './databaseLoader';
import { Container } from 'typescript-ioc';

export default async ({ expressApp }: { expressApp: express.Application }) => {
	/**
  * se carga la base de datos
  */
  Container.get(DatabaseConnection);
  /**
  * Se cargan todas las rutas
  */
  console.log('cargando rutas');
  return routesLoader({ app: expressApp });
};