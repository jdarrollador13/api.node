//import Pool  from 'pg-pool';
const pg  = require('pg')
import { Singleton } from 'typescript-ioc';
/**
 * @category Database
 */
@Singleton
export default class DatabaseConnection {
    private pool:any
    constructor() {
        const config = {
          user: 'postgres',
          password: 'admin',
          host: 'localhost',
          port: 5432,
          database: 'prueba',
          ssl: false
        };
        this.pool = pg.Pool(config).connect().then((pool:any) => {
            console.info('Connected to postgres');
            return pool;
        }).catch((err:any) => {
            console.error('Database Connection Failed! Bad Config: ', err);
            throw 'No hay conexión a la BD!!';
        });
    }
    public getPool() {
        return this.pool;
    }
}