import { Inject, Container } from "typescript-ioc"
import  ListarPersonasDAO  from '../DAO/ListarPersonasDAO'
export default class ListarPersonasController {
	private response:object|any
	private objectResponse:object|any 
	private resController:object|any
	contructor() { 
		this.response = {}
		this.objectResponse = {}
		this.resController = {}
	}

	async obtenerListadoPersonas(request:object|any):Promise<object|any> {
		let parametros:string|any
		let colums:string|any
		colums = ''
		parametros = request.body
		if(parametros.Nombre){
			colums+=`,"Informacio"->'informacion'->>'Nombre' AS Nombre`
		}
		if(parametros.Apellido){
			colums+=`,"Informacio"->'informacion'->>'Apellido' AS Apellido`
		}
		if(parametros.Ciudad){
			colums+=`,"Informacio"->'informacion'->>'Ciudad' AS Ciudad`
		}
		if(parametros.Direccion){
			colums+=`,"Informacio"->'informacion'->>'Direccion' AS Direccion`
		}
		if(parametros.Telefono){
			colums+=`,"Informacio"->'informacion'->>'Telefono' AS Telefono`
		}
		const listarPersonasDAO:ListarPersonasDAO = Container.get(ListarPersonasDAO)
		this.response = await listarPersonasDAO.listarPersonas(colums)
		if(this.response.rowCount > 0 ){
			this.objectResponse = this.response.rows
		}else{
			this.objectResponse = this.response.name
		}
		this.resController = {
			status : (this.response.rowCount) ? 200 : 500,
			data : this.objectResponse 
		}
		return this.resController
	}
}