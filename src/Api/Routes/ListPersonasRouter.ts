import { Router, Response, Request, NextFunction } from 'express'
import ListarPersonasController from '../../Controller/ListarPersonasController'
import { Container } from "typescript-ioc";

export default class ListPersonasRouter {

	private app:Router
	constructor(router: Router) {
		this.app = router
	}

	router():void {
		this.app.post('/listar/personas',
			async (req:Request, res:Response, next:NextFunction) => {
				const listarPersonasController:ListarPersonasController = Container.get(ListarPersonasController)
				let resp = await listarPersonasController.obtenerListadoPersonas(req)
				res.status(200).json(resp)
			}
		)
	}
}