import { Router } from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import cors from 'cors'
import ListPersonasRouter from './routes/ListPersonasRouter'

class GeneralRouter {
  public csrfProtection:any
  public router:Router
  private listPersonasRouter:any

  constructor() {
    this.router = Router()
    this.config()
    this.routes()
  }
  routes(){
    this.listPersonasRouter.router()
  }
  config(){
    this.router.use(bodyParser.json());
    this.router.use(bodyParser.urlencoded({ extended: true }));
    this.router.use(morgan('dev'))
    this.router.use(cors({
      'allowedHeaders': ['sessionId', 'Content-Type'],
      'exposedHeaders': ['sessionId'],
      'origin': '*',
      'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
      'preflightContinue': false
    }))
    this.listPersonasRouter = new ListPersonasRouter(this.router)
  }
}
const GeneralRouters =  new GeneralRouter
export default GeneralRouters.router