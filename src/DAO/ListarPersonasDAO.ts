import { Inject, Container } from "typescript-ioc"
import DatabaseConnection from "../Loaders/databaseLoader";
export default class ListarPersonasDAO {
	private response:object|any
	private sql:string|any
	constructor(@Inject private databaseConnection:DatabaseConnection){
		this.response = {}
	}

	async listarPersonas(colums:object|any):Promise<object|any>{
		try{
			//console.log(colums,`SELECT "IdPersonal", ${colums} FROM  public.personal WHERE "Activo" = '1'`)
			const connect = await this.databaseConnection.getPool()
			this.sql = await connect.query(`SELECT "IdPersonal" ${colums}
																			FROM  public.personal WHERE "Activo" = '1'`)
			this.response = this.sql
		}catch(error){
			this.response = error
		}finally{
			console.log(this.response)
			return this.response
		}
	}

}