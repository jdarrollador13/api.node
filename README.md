-- Database: prueba

-- DROP DATABASE prueba;

CREATE DATABASE prueba
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Colombia.1252'
    LC_CTYPE = 'Spanish_Colombia.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


-- SEQUENCE: public."personal_IdPersonal_seq"

-- DROP SEQUENCE public."personal_IdPersonal_seq";

CREATE SEQUENCE public."personal_IdPersonal_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public."personal_IdPersonal_seq"
    OWNER TO postgres;


-- Table: public.personal

-- DROP TABLE public.personal;

CREATE TABLE public.personal
(
    "IdPersonal" integer NOT NULL DEFAULT nextval('"personal_IdPersonal_seq"'::regclass),
    "Nombre" text COLLATE pg_catalog."default" NOT NULL,
    "Apellido" text COLLATE pg_catalog."default" NOT NULL,
    "Ciudad" text COLLATE pg_catalog."default",
    "Direccion" text COLLATE pg_catalog."default",
    "Telefono" text COLLATE pg_catalog."default",
    "Informacio" jsonb NOT NULL,
    "Activo" bigint NOT NULL DEFAULT 1,
    CONSTRAINT personal_pkey PRIMARY KEY ("IdPersonal")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.personal
    OWNER to postgres;

INSERT INTO public.personal(
	"Nombre", "Apellido", "Ciudad", "Direccion", "Telefono", "Informacio")
	VALUES ('jonathan', 'pinto', 'Bogota', 'calle 30d 8 12 sur ', '3168749106', '{ "informacion" : {"Nombre" : "jonathan", "Apellido" : "pinto", "Ciudad" : "Bogota", "Direccion" : "calle 30d 8 12 sur", "Telefono" : "3168749106" }}');



============== Ejecutar comandos ===============
postgres Version 11.7.1
=============Proyecto Angular===================
npm run start 
=============Protecto Node======================
npm run dev
=============Proyecto Flask=====================
python aplicacion.py
